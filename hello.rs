//TODO: Add header

mod gitlab;

use gitlab::common::hello as hello;

fn main() {
    println!("{}", gitlab::common::hello()); // implicit conversion from String to str

    println!("{}", hello()); // needs the use as aliasing above 

    println!("{} days", 32);

    println!("{number:>width$}", number=1, width=6);

    let mut keksi: bool = true; // TODO: Make this an input from the CLI 

    if keksi == true {
        println!("I love Keksi {}", keksi);
    } else {
        println!("ENOKEKSI");
    }

    keksi = false;

    let tuple_of_tuples = ((1u8, 2u16, 2u32), (4u64, -1i8), -2i16);

    // Tuples are printable
    println!("tuple of tuples: {:?}", tuple_of_tuples);

    // Release printer
    println!("New GitLab release on the {}", gitlab::release::print(10)); //TODO: Replace with "DateTime now -> returning month"
}


