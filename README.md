# gitpod-learn-rust

Slides: https://docs.google.com/presentation/d/1t1FdHh04TAOg9WITqRFJHz1YFxMbsQeekN8th1UfFcI/edit 

## Rust Modules

https://doc.rust-lang.org/rust-by-example/mod/split.html 

```
$ tree .
.
|-- gitlab
|   |-- mod.rs # entrypoint declaration
|   |-- common.rs # functions (public)
|   |-- release.rs # functions (public)
`-- hello.rs
```



More insights:

- [Rust modules explained](https://medium.com/@tak2siva/rust-modules-explained-96809931bbbf)